# Starting from base image php-fpm-alpine
FROM php:fpm-alpine3.18

# Install nginx
RUN apk add nginx

RUN docker-php-ext-install mysqli

# COPY php files into /var/www/php_crud
COPY . /var/www/php_crud

# COPY server_block into /etc/nginx/http.d/php_crud.conf
COPY ./php_crud.conf /etc/nginx/http.d/php_crud.conf

#RUN php-fpm -D this doesn't really work properly

# EXPOSE PORT 9090
EXPOSE 9090

# CMD nginx -g "daemon off;"
CMD php-fpm -D;nginx -g "daemon off;"